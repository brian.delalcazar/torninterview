import React, { useState } from "react";
import "./App.css";
import MainColumn from "./MainColumn";

function App() {
  const [mainName, setMainName] = useState("");
  const [mainColumns, setMainColumns] = useState<
    { name: string; items: string[] }[]
  >([]);

  const addNewColumn = () => {
    if (!mainName) return;
    setMainColumns([...mainColumns, { name: mainName, items: [] }]);
    setMainName("");
  };

  const move = (fromIndex: number, toIndex: number, itemIndex: number) => {
    const allColumns = [...mainColumns];
    const item = allColumns[fromIndex].items.splice(itemIndex, 1)?.[0];
    allColumns[toIndex].items.splice(itemIndex, 0, item);
    setMainColumns(allColumns);
  };

  const addNewItem = (name: string, index: number) => {
    const allColumns = [...mainColumns];
    if (!name) return;
    allColumns[index].items.push(name);
    setMainColumns(allColumns);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          gap: "15px",
          marginBottom: "25px",
          flexWrap: "wrap",
          margin: "15px",
        }}
      >
        {mainColumns.map((column, index) => {
          return (
            <MainColumn
              name={column.name}
              items={column.items}
              index={index}
              onAddNewItem={(name: string) => addNewItem(name, index)}
              isFirst={index === 0}
              isLast={index + 1 === mainColumns.length}
              onMoveItem={move}
            />
          );
        })}
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <input
          style={{
            marginBottom: "10px",
          }}
          type="text"
          value={mainName}
          onChange={(e: any) => setMainName(e.target.value)}
        />
        <button onClick={() => addNewColumn()}>Add new column</button>
      </div>
    </div>
  );
}

export default App;
