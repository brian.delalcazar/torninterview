const MainColumn = ({
  isFirst,
  isLast,
  index,
  items,
  name,
  onAddNewItem,
  onMoveItem,
}: {
  isFirst: boolean;
  isLast: boolean;
  index: number;
  items: string[];
  name: string;
  onAddNewItem: (name: string) => void;
  onMoveItem: (fromIndex: number, toIndex: number, itemIndex: number) => void;
}) => {
  const addNewItem = () => {
    const itemName = prompt("Task:");
    if (!itemName) return;
    onAddNewItem(itemName);
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        background: "#f7afaf",
        padding: "0px 5px 5px 5px",
        width: "250px",
      }}
    >
      <div
        style={{
          width: "100%",
          background: "#8888f7",
          textAlign: "center",
          padding: "5px",
          marginBottom: "10px",
        }}
      >
        {name}
      </div>
      {items.map((item, itemIndex) => {
        return (
          <div
            style={{
              gap: "10px",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "100%",
              marginBottom: "10px",
            }}
          >
            <button
              style={{
                border: "none",
                color: "#6088d8",
                background: "transparent",
                fontSize: "16px",
              }}
              disabled={isFirst}
              onClick={() => {
                onMoveItem(index, index - 1, itemIndex);
              }}
            >
              {" "}
              &#60;{" "}
            </button>
            <div
              style={{
                maxWidth: "80%",
                wordBreak: "break-all",
              }}
            >
              {item}
            </div>
            <button
              style={{
                border: "none",
                color: "#6088d8",
                background: "transparent",
                fontSize: "16px",
              }}
              disabled={isLast}
              onClick={() => {
                onMoveItem(index, index + 1, itemIndex);
              }}
            >
              {" "}
              &#62;{" "}
            </button>
          </div>
        );
      })}
      <button
        style={{
          marginTop: "10px",
        }}
        onClick={addNewItem}
      >
        + Add a task
      </button>
    </div>
  );
};

export default MainColumn;
